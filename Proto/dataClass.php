<?php

class DebuggerData
{
    private array $show = [];
    private array $showln = [];
    private array $debug = [];
    private array $success = [];
    private array $failure = [];
    private array $warning = [];

    private DateTimeImmutable $executionStart;
}
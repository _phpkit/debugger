<?php

namespace Prophp;

class Config
{
    private bool $handleErrorsExceptions = false;
    private bool $handleWarnings = false;
    private bool $exitOnWarning = false;
    // @todo rootPath = '/var/www/html'

    /**
     * @return bool
     */
    public function isHandleErrorsExceptions(): bool
    {
        return $this->handleErrorsExceptions;
    }

    /**
     * @param bool $handleExceptions
     */
    public function setHandleErrorsExceptions(bool $handleExceptions): void
    {
        $this->handleErrorsExceptions = $handleExceptions;
    }

    /**
     * @return bool
     */
    public function isHandleWarnings(): bool
    {
        return $this->handleWarnings;
    }

    /**
     * @param bool $handleWarnings
     */
    public function setHandleWarnings(bool $handleWarnings): void
    {
        $this->handleWarnings = $handleWarnings;
    }

}
<?php

namespace Prophp;

class Handler
{
    static private function handleExceptionsErrors()
    {
        $nl = PHP_EOL;
        set_error_handler(function ($errno, $errstr, $errfile, $errline) use ($nl) {

            $backtrace = debug_backtrace(/*DEBUG_BACKTRACE_IGNORE_ARGS*/);

            $caller = $backtrace[1];

            $label = _::styles()::warningLabel() . " WARNING " . _::styles()::labelEnd();
            $callerRef = _::formatter()::prepareCallerReferenceString($caller, false);
            $msg = _::formatter()::prepareMessageString($errstr);

            echo "$label $callerRef{$nl}▸ $msg$nl";

            foreach (array_slice($backtrace, 1) as $caller) {
                echo "◦ " . _::formatter()::prepareCallerReferenceString($caller) . $nl;
            }
        }, E_ALL);
    }

    static private function handleWarnings()
    {
        $nl = PHP_EOL;
        set_exception_handler(function (\Error|\Exception $e) use ($nl) {

            $label = _::styles()::errorLabel() . " " . strtoupper(get_class($e)) . " " . _::styles()::labelEnd();
            $callerRef = _::formatter()::prepareExceptionCallerReferenceString($e);
            $msg = _::formatter()::prepareExceptionMessageString($e);

            echo "$label $callerRef{$nl}▸ $msg$nl";

            foreach ($e->getTrace() as $caller) {
                echo "◦ " . _::formatter()::prepareCallerReferenceString($caller) . $nl;
            }
        });
    }

    static public function setHandlers()
    {
        if (_::config()::isHandlingWarnings()) {
            self::handleWarnings();
        }


        if (_::config()::isHandlingErrorsExceptions()) {
            self::handleExceptionsErrors();
        }
    }
}

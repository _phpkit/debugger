<?php

namespace Prophp;

// @link https://gist.github.com/vratiu/9780109
class Styles
{
    public static function labelEnd()
    {
        return "\e[0m";
    }

    private static function boldRed_bgBlack()
    {
        return "\e[1;31;40m";
    }

    private static function boldYellow_bgBlack()
    {
        return "\e[1;33;40m";
    }

    private static function boldWhite()
    {
        return "\e[1;38m";
    }

    private static function grey(){
        return "\e[0;37m";
    }

    private static function boldGrey(){
        return "\e[1;37m";
    }

    # MAP

    public static function errorLabel()
    {
        return self::boldRed_bgBlack();
    }

    public static function warningLabel()
    {
        return self::boldYellow_bgBlack();
    }

    public static function message()
    {
        return self::boldWhite();
    }

    public static function backtraceCaller(){
        return self::grey();
    }

    public static function backtraceArgs(){
        return self::boldGrey();
    }

    public static function mainCaller(){
        return self::boldGrey();
    }
}
<?php

namespace Prophp;

// @todo Implement skeleton pattern in order not to initialize new objects

class ConfigManager
{
    public static function getDefaultConfigFilePath()
    {
        return dirname(__DIR__) . DIRECTORY_SEPARATOR . "debugger.config.json";
    }

    // @todo
    public function getConfigFilePath()
    {
        // might be in 'root/' or 'root/config/'. Naming also same: debugger.config.json
    }


    private static function getInstance(): Config
    {
        if (array_key_exists('Debugger', $GLOBALS)) {
            return $GLOBALS['Debugger'];
        }

        return $GLOBALS['Debugger'] = new Config();
    }

    static function handleErrorsExceptions(bool $bool = true): self
    {
        self::getInstance()->setHandleErrorsExceptions($bool);
        return new static;
    }

    static function isHandlingErrorsExceptions(): bool
    {
        return self::getInstance()->isHandleErrorsExceptions();
    }

    static function handleWarnings(bool $bool = true): self
    {
        self::getInstance()->setHandleWarnings($bool);
        return new static;
    }

    static function isHandlingWarnings(): bool
    {
        return self::getInstance()->isHandleWarnings();
    }
}
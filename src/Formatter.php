<?php

namespace Prophp;

class Formatter
{
    private static $rootPath = null;

    private static function getRootPath()
    {
        // @todo Use user config first. If this one is absent, use a default one
        if (self::$rootPath === null) {
            self::$rootPath = json_decode(file_get_contents(_::config():: getDefaultConfigFilePath()))->rootPath;
        }

        return self::$rootPath;
    }

    public static function prepareArgsString($args = null)
    {
        $args = $args ?? [];

        array_walk($args, function (&$value) {
            if (is_string($value)) {
                $value = "\"$value\"";
            }

            if (is_array($value)) {
                $value = json_encode($value);
            }

            if (is_object($value)) {
                ob_start();
                var_dump($value);
                $var_dump = ob_get_clean();
                if (php_sapi_name() === 'cli') {
                    $value = substr($var_dump, 0, 100) . " ...";
                }
            }
        });

        return implode(", ", $args);
    }

    public static function prepareExceptionCallerReferenceString(\Error|\Exception $e)
    {
        $filePath = str_replace(self::getRootPath(), "", $e->getFile());

        return _::styles()::mainCaller() . "$filePath:{$e->getLine()}" . _::styles()::labelEnd();
    }

    public static function prepareExceptionMessageString(\Error|\Exception $e)
    {
        return _::styles()::message() . "{$e->getMessage()}" . _::styles()::labelEnd();
    }


    public static function prepareCallerReferenceString(array $caller, bool $detailed = true)
    {
        if ($detailed) {
            if (array_key_exists('file', $caller)) {

                $filePath = str_replace(self::getRootPath(), "", $caller['file']);

                return _::styles()::backtraceCaller() . "$filePath:{$caller['line']}" . _::styles()::labelEnd() .
                    " {$caller['function']}(" .
                    _::styles()::backtraceArgs() .
                    _::formatter()::prepareArgsString($caller['args'] ?? null) .
                    _::styles()::labelEnd() .
                    ")";
            }

            return _::styles()::backtraceCaller() . "{closure}" . _::styles()::labelEnd();
        }

        if (array_key_exists('file', $caller)) {
            $filePath = str_replace(self::getRootPath(), "", $caller['file']);

            return _::styles()::mainCaller() . "$filePath:{$caller['line']}" . _::styles()::labelEnd();;
        }

        return _::styles()::mainCaller() . "{closure}" . _::styles()::labelEnd();
    }

    public static function prepareMessageString(string $msg)
    {
        return _::styles()::message() . $msg . _::styles()::labelEnd();
    }
}
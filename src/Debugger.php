<?php

namespace Prophp;

class Debugger
{
    private static $config = null;
    private static $formatter = null;
    private static $styles = null;
    private static $handler = null;

    // @todo $userConfig

    static public function config(): ConfigManager
    {
        self::$config = self::$config ?? new ConfigManager();
        return self::$config;
    }

    static public function formatter(): Formatter
    {
        self::$formatter = self::$formatter ?? new Formatter();
        return self::$formatter;
    }

    static public function styles(): Styles
    {
        self::$styles = self::$styles ?? new Styles();
        return self::$styles;
    }

    static public function handler(): Handler
    {
        self::$handler = self::$handler ?? new Handler();
        return self::$handler;
    }
}
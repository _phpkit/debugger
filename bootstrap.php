<?php

require_once "vendor/autoload.php";

use Prophp\Debugger;

if (php_sapi_name() === 'cli') {
    Debugger::config()
        ::handleErrorsExceptions()
        ::handleWarnings();
}

Debugger::handler()::setHandlers();
